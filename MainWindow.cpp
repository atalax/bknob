#include "MainWindow.h"

#include "BKnob.h"
#include <Application.h>
#include <Message.h>
#include <Slider.h>
#include <Button.h>
#include <LayoutBuilder.h>

#include <iostream>

using namespace std;

MainWindow::MainWindow(void)
	:
	BWindow(BRect(100, 100, 500, 400), "Main Window",
		B_TITLED_WINDOW, B_ASYNCHRONOUS_CONTROLS)
{
	BKnob* knobMarks = new BKnob("knob", "Normal",
			new BMessage(B_CONTROL_MODIFIED), 0, 9);
	knobMarks->SetHashMarkCount(10);

	BKnob* knobSmall = new BKnob("knob", "Small",
			new BMessage(B_CONTROL_MODIFIED), 0, 6, 60);
	knobSmall->SetHashMarkCount(7);

	BKnob* knobSmooth = new BKnob("knob", "Smooth",
			new BMessage(B_CONTROL_MODIFIED), 0, 100);
	knobSmooth->SetWheelDelta(5);

	BKnob* knobBig = new BKnob("knob", "BIG",
			new BMessage(B_CONTROL_MODIFIED), 0, 3, 100);
	knobBig->SetHashMarkCount(4);

	BKnob* knobDisabled = new BKnob("bknob", "Disabled",
			new BMessage(B_CONTROL_MODIFIED), 0, 9);
	knobDisabled->SetHashMarkCount(10);
	knobDisabled->SetValue(6);
	knobDisabled->SetEnabled(false);

	BLayout* l = BLayoutBuilder::Grid<>(this)
					.SetInsets(B_USE_WINDOW_INSETS)
					.Add(knobMarks, 0, 0)
					.Add(knobSmall, 0, 1)
					.Add(knobSmooth, 1, 0)
					.Add(knobBig, 1, 1)
					.Add(knobDisabled, 2, 0);

	BSize size = l->PreferredSize();
	ResizeTo(size.Width(), size.Height());
}


bool MainWindow::QuitRequested()
{
	be_app->PostMessage(B_QUIT_REQUESTED);
	return true;
}


void MainWindow::MessageReceived(BMessage* msg)
{
	switch (msg->what) {
	case B_CONTROL_MODIFIED:
		cout << msg->GetInt32("be:value", -1) << endl;
		break;
	default:
		BWindow::MessageReceived(msg);
	}
}
