/*
 * Copyright 2011 Davide Gessa.
 * Copyright 2014 Josef Gajdusek.
 * All rights reserved. Distributed under the terms of the MIT license.
 *
 * Authors:
 *				David Gessa, dak.linux@gmail.com
 *				Josef Gajdusek, atx@atx.name
 */
#ifndef BKNOB_H
#define BKNOB_H

#include <Control.h>
#include <Slider.h>

class BKnob : public BSlider {
public:
							BKnob(BRect frame, const char* name,
								const char* label, BMessage* message,
								int32 minValue, int32 maxValue,
								int32 diameter = 80);
							BKnob(const char* name, const char* label,
								BMessage* message, int32 minValue,
								int32 maxValue, int32 diameter = 80);
							BKnob(BMessage* archive);

	virtual status_t		Archive(BMessage* archive, bool deep = true) const;

	virtual void			MessageReceived(BMessage* msg);

	virtual void			Draw(BRect updateRect);

	virtual void			MouseMoved(BPoint point, uint32 transit,
								const BMessage* dragMessage);

	virtual int32			ValueForPoint(BPoint point) const;
	virtual void			SetValue(int32 value);

	virtual BSize			MinSize();
	virtual BSize			MaxSize();
	virtual BSize			PreferredSize();
	virtual BRect			BarFrame() const;

	virtual void			SetDiameter(int32 diameter);
	virtual int32			Diameter();

	virtual void			SetWheelDelta(int32 delta);
	virtual int32			WheelDelta();

private:
	virtual BPoint			_Center() const;

	int32					fDiameter;
	int32					fWheelDelta;
};

#endif
