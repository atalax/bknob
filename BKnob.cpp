/*
 * Copyright 2011 Davide Gessa. 
 * Copyright 2014 Josef Gajdusek.
 * All rights reserved. Distributed under the terms of the MIT license.
 *
 * Authors:
 *				David Gessa, dak.linux@gmail.com
 *				Josef Gajdusek, atx@atx.name
 */
#include "BKnob.h"

#include <stdio.h>
#include <Layout.h>
#include <Message.h>
#include <GradientLinear.h>
#include <stdlib.h>
#include <math.h>

#define CUTOUT (M_PI * 0.75)

BKnob::BKnob(BRect frame, const char* name, const char* label,
		BMessage* message, int32 minValue, int32 maxValue,
		int32 diameter)
	:
	BSlider(frame, name, label, message, minValue, maxValue),
	fDiameter(diameter),
	fWheelDelta(1)
{
}


BKnob::BKnob(const char* name, const char* label, BMessage* message,
		int32 minValue, int32 maxValue, int32 diameter)
	:
	BSlider(name, label, message, minValue, maxValue, B_VERTICAL),
	fDiameter(diameter),
	fWheelDelta(1)
{
}


BKnob::BKnob(BMessage* archive)
	:
	BSlider(archive)
{
	fDiameter = archive->GetInt32("_diameter", 80);
}


status_t
BKnob::Archive(BMessage* archive, bool deep) const
{
	status_t ret = BSlider::Archive(archive, deep);

	if (ret == B_OK)
		ret = archive->AddInt32("_diameter", fDiameter);

	return ret;
}


void
BKnob::MessageReceived(BMessage* msg)
{
	switch(msg->what) {
		case B_MOUSE_WHEEL_CHANGED:
		{
			if (fWheelDelta != 0 && IsEnabled()) {
				float deltaY = msg->GetFloat("be:wheel_delta_y", 0.0);
				if (deltaY != 0.0 && fWheelDelta != 0) {
					int32 old = Value();
					SetValue(Value() + deltaY * fWheelDelta);
					if (old != Value())
						Invoke();
				}
			}
		}
		default:
			BSlider::MessageReceived(msg);
	}
}


void
BKnob::Draw(BRect updateRect)
{
	// Get Max and Min values
	int32 max;
	int32 min;
	GetLimits(&min, &max);
	int32 valCount = max - min + 1;

	int radius = fDiameter / 2;
	BPoint center = _Center();

	FillRect(Bounds(), B_SOLID_LOW);
	SetDrawingMode(B_OP_ALPHA);

	// Draw circles
	BGradientLinear gradient(center.x, center.y + radius * 0.6,
		center.x, center.y - radius * 0.6);
	gradient.AddColor(
		tint_color(ui_color(B_PANEL_BACKGROUND_COLOR), B_DARKEN_1_TINT),
		0);
	gradient.AddColor(ui_color(B_PANEL_BACKGROUND_COLOR), 40);
	gradient.AddColor(
		tint_color(ui_color(B_PANEL_BACKGROUND_COLOR), B_LIGHTEN_2_TINT),
		255);
	FillEllipse(center, radius * 0.6, radius * 0.6, gradient);

	rgb_color outlineCol = ui_color(B_SHADOW_COLOR);
	outlineCol.alpha = 150;
	SetHighColor(outlineCol);
	StrokeEllipse(center, radius * 0.6, radius * 0.6, B_SOLID_HIGH);

	SetHighColor(
		tint_color(ui_color(B_PANEL_BACKGROUND_COLOR), B_LIGHTEN_2_TINT));
	FillEllipse(center, radius * 0.3, radius * 0.3, B_SOLID_HIGH);
	SetHighColor(outlineCol);
	StrokeEllipse(center, radius * 0.3, radius * 0.3, B_SOLID_HIGH);

	if (!IsEnabled()) {
		rgb_color disabledCol = tint_color(ui_color(B_PANEL_BACKGROUND_COLOR),
			B_DARKEN_3_TINT);
		disabledCol.alpha = 60;
		SetHighColor(disabledCol);
		FillEllipse(center, radius * 0.6, radius * 0.6, B_SOLID_HIGH);
	}

	// Transform Value to radians
	float angle = ((float) Value() + 0.5) / valCount * 2 * CUTOUT - CUTOUT;
	float deltaX = cos(angle  - M_PI / 2);
	float deltaY = sin(angle  - M_PI / 2);

	// Draw circle marking the selected variable
	BPoint pointerLoc = center;
	pointerLoc.x += radius * 0.45 * deltaX;
	pointerLoc.y += radius * 0.45 * deltaY;
	rgb_color pointerCol =
		tint_color(ui_color(B_PANEL_BACKGROUND_COLOR), B_DARKEN_2_TINT);
	SetHighColor(pointerCol);
	FillEllipse(pointerLoc, radius * 0.05, radius * 0.05);
	SetHighColor(tint_color(pointerCol, B_DARKEN_2_TINT));
	StrokeEllipse(pointerLoc, radius * 0.05, radius * 0.05);

	// Draw the ticks
	SetPenSize(1);
	rgb_color tickColor = ui_color(B_SHADOW_COLOR);
	tickColor.alpha = 127;
	SetHighColor(tickColor);
	double anglePerTick = 2 * CUTOUT / HashMarkCount();
	for (int i = 0; i < HashMarkCount(); i++) {
		float angle = i * anglePerTick- M_PI / 2 + CUTOUT + anglePerTick / 2;
		BPoint start = center;
		BPoint end = center;
		deltaX = sin(angle);
		deltaY = cos(angle);
		start.x += radius * 0.7 * deltaX;
		start.y += radius * 0.7 * deltaY;
		end.x += radius * 0.8 * deltaX;
		end.y += radius * 0.8 * deltaY;
		StrokeLine(start, end);
	}

	DrawText();
}


void
BKnob::MouseMoved(BPoint point, uint32 transit, const BMessage* message)
{
	int32 val = ValueForPoint(point);
	int32 min;
	int32 max;
	GetLimits(&min, &max);
	if (abs(val - Value()) > (max - min) / 2.0)
		return;
	BSlider::MouseMoved(point, transit, message);
}


int32
BKnob::ValueForPoint(BPoint p) const
{
	BPoint center = _Center();
	float deltaX = center.x - p.x;
	float deltaY = center.y - p.y;
	float angle = -atan2(deltaX, deltaY);

	int32 max;
	int32 min;
	GetLimits(&min, &max);

	if (fabs(angle) > CUTOUT)
		return Value();

	return (max - min + 1) * (angle + CUTOUT) / (2 * CUTOUT) + min;
}


void
BKnob::SetValue(int32 value)
{
	int32 old = Value();
	BSlider::SetValue(value);
	if (old != Value())
		Invalidate();
}


BSize
BKnob::MinSize()
{
	return PreferredSize();
}


BSize
BKnob::MaxSize()
{
	return PreferredSize();
}


BSize
BKnob::PreferredSize()
{
	font_height fontHeight;
	GetFontHeight(&fontHeight);
	return BSize(fDiameter,
		fDiameter +
		(Label() != NULL ?
		 (fontHeight.ascent + fontHeight.descent) * 1.25 : 0));
}


BRect
BKnob::BarFrame() const
{
	BPoint center = _Center();
	float radius = fDiameter / 2;
	return BRect(center.x - radius, center.y - radius,
		center.x + radius, center.y + radius);
}


void
BKnob::SetDiameter(int32 diameter)
{
	fDiameter = diameter;
	Invalidate();
}


int32
BKnob::Diameter()
{
	return fDiameter;
}


void
BKnob::SetWheelDelta(int32 delta)
{
	fWheelDelta = delta;
}


int32
BKnob::WheelDelta()
{
	return fWheelDelta;
}


BPoint
BKnob::_Center() const
{
	BPoint ret = Bounds().RightBottom();
	ret.x /= 2;
	ret.y /= 2;
	return ret;
}
